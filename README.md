# Oracle Database Sample Schemas

UPD:  
I found original repo with sample schemas from oracle repository:  
https://github.com/plsql/db-sample-schemas
___

Демонстрационные Схемы

Схема SCOTT/TIGER имеется в наличии по умолчанию в Oracle Database10g EE (правда, в Database10g XE ее надо заводить отдельно);

    HR (Human resources - кадры, людские ресурсы) - основные темы, поддержка Oracle Internet Directory;
    OE (Order entry - ввод заказов) - промежуточные темы, различные типы данных (datatypes);
    PM (Product media - продуктовая среда) - типы данных (datatypes), используемые для мультимедиа;
    QS (Queued shipping - постановка в очередь) - продвинутая организация очередей, назвываемых IX в 10g;
    SH (Sales history - история продаж) - большое количество данных, аналитическая обработка.

Существует также подсхема, называемая OC (для оперативного (Online) Catalog, использующегося в схеме OE), используемая для объектно-реляционных (object-relational) примеров.

----

Все варианты инсталяции (personal, standard или enterprise) включают первые пять схем, и только конфигурация ЕЕ (enterprise edition), включающая опцию секционирования (partitioning), обеспечивается схемой SH.


=========

### Example how to run this script:

    $ cd /tmp
    $ git clone https://github.com/plsql/Oracle-Database-Sample-Schemas
    $ cd Oracle-Database-Sample-Schemas
    $ cd my_version_edited
    $ sqlplus / as sysdba
    SQL> @hr.sql

=========

## To get more information please read readme.txt

